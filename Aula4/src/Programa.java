
public class Programa {

	public static void main(String[] args) {

           Conta C1 = new Conta(0123, 23000, true);
           Conta C2 = new Conta(4567, 25000, true);
           
           	
           System.out.println("Saldo " + C1.getSaldo());  
           System.out.println("Saldo " + C2.getSaldo());
           
           System.out.println("**********************************");
           
           C1.depositar(2500); //modifica o estado do objeto 
           System.out.println("Numero: " + C1.getNumero());
           System.out.println("Saldo apos o deposito: " + C1.getSaldo());
                      
           System.out.println("**********************************");
           
           C2.depositar(100);   
           System.out.println("Numero: " + C2.getNumero());
           System.out.println("Saldo apos deposito: " + C2.getSaldo());
           
           
           
           

	}

}
