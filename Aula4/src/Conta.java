
public class Conta {
	
	private static int contador = 1000; //privado neste caso, somente ser�o aceitos os metodos nesta mesma classe.
	
	//atributos de instancia
	private int numero;
	private double saldo;
	private boolean ativa;
	
	
	
	//construtor 
	public Conta(int num, double saldo) {
		numero = contador++; 
		this.saldo = saldo;//this. serve para dizer que o conteudo da variavel sera atribuido a classe de instancia.
		
	}
	
	//sobrecarga do construtor
	public Conta (int numero, double saldo, boolean status) {
		this.numero = contador++; 
		this.saldo = saldo;
		this.ativa = status;
		
	}
	
	
	//m�todos de acessos publicos
	public int getNumero() {
		return numero;
	}
	
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	
	//metodo de neg�cio
	public void sacar (double num) {
		this.saldo -= num;
	}
	
	public void depositar(double num) {
		this.saldo += num;
	}
	
	
	
	

}
